package pl.cm.dj;

import pl.cm.dj.io.InputReader;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageBuilder;
import pl.cm.dj.model.MessageType;
import pl.cm.dj.repository.MessageRepository;
import pl.cm.dj.repository.MessageSimpleRepository;
import pl.cm.dj.repository.MessageSimpleRepositoryAdapter;
import pl.cm.dj.repository.MessageSimpleStorage;
import pl.cm.dj.service.MessageFillerService;
import pl.cm.dj.service.MessageService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {


    MessageSimpleStorage storage;
    MessageSimpleRepository simpleRepository;
    MessageSimpleRepositoryAdapter simpleRepositoryAdapter;
    MessageService messageService;
    InputReader inputReader;

    MessageFillerService messageFillerService;
    String messageData;

    public Main() {
        this.storage = new MessageSimpleStorage();
        this.simpleRepository = new MessageSimpleRepository(storage);
        this.simpleRepositoryAdapter = new MessageSimpleRepositoryAdapter(simpleRepository);
        this.messageService = new MessageService(simpleRepositoryAdapter);
        this.inputReader = new InputReader();
        this.messageFillerService = new MessageFillerService(simpleRepositoryAdapter);
    }

    public void init() {
        messageFillerService.fillRepository(10);
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.init();
        main.start();
    }

    public void start() {
        boolean running = true;
        while (running) {
            showBaseMenu();
            messageData = inputReader.getUserInput("Select option:");
            running = userBaseInteraction(messageData);
        }
    }

    public static void showBaseMenu() {
        System.out.println("1.Add new message");
        System.out.println("2.Delete message");
        System.out.println("3.Show all messages by Title & Author");
        System.out.println("4.Show all messages");
        System.out.println("5.Filer messages");
        System.out.println("6.Exit");
    }

    public boolean userBaseInteraction(String messageData) {
        switch (messageData) {
            case "1":
                addNewMessage();
                break;
            case "2":
                deleteMessage();
                break;
            case "3":
                showAllMessageTitleAndAuthor();
                break;
            case "4":
                showAllMessageFullInformation();
                break;
            case "5":
                filterMessages();
                break;
            case "6":
                return false;
        }
        return true;
    }

    public void filterMessages() {
        showFilterInformation();
        Message message = getBaseMessageBuilder().build();
        List<Message> messages = filterMessagesByInput(message);
        messages.forEach(System.out::println);
    }

    public List<Message> filterMessagesByInput(Message message) {
        return messageService.findByFilters(message);
    }

    public void showFilterInformation() {
        System.out.println("Filter by Author and Title and Content.\n" +
                "Filtering is with logical operator and.\n" +
                "If you want filter only by title input title, other field leave empty(by pressing Enter).\n" +
                "You can use filters in same way for other fields.\n" +
                "Also you can filter by using all possible field combinations.");
    }

    public void showAllMessageFullInformation() {
        simpleRepositoryAdapter.getAll().forEach(System.out::println);
    }

    public void addNewMessage() {
        simpleRepositoryAdapter.add(scannerMessageBuild());
    }

    public void showAllMessageTitleAndAuthor() {
        simpleRepositoryAdapter.getAll()
                .stream()
                .forEach(message -> {
                    System.out.println(message.toSimpleString());
                });
    }

    public void deleteMessage() {
        showDeleteInformation();
        Message message = getBaseMessageBuilder().build();
        boolean isRemoved = deleteByMessagePropertiesThatAreNotNull(message);
        if (isRemoved) {
            System.out.println("Removed Successfully");
        } else {
            System.out.println("Not such a message to remove!");
        }
    }

    public boolean deleteByMessagePropertiesThatAreNotNull(Message message) {
        return messageService.remove(message);
    }

    public static void showDeleteInformation() {

        System.out.println("Delete by Author and Title and Content.\n" +
                "Deleting is with logical operator and.\n" +
                "If you want delete only by title input title, other field leave empty(by pressing Enter).\n" +
                "You can use deleting in same way for other fields.\n" +
                "Also you can delete by using all possible field combinations.");
    }


    public Message scannerMessageBuild() {
        MessageBuilder machineBuilder = getBaseMessageBuilder();
        machineBuilder.withCreationDate(LocalDateTime.now());
        messageData = inputReader.getUserInput("Enter type 1- IMPORTANT, 2-PRIVATE:");
        if ("1".equals(messageData)) {
            machineBuilder.withMessageType(MessageType.IMPORTANT);
        } else {
            machineBuilder.withMessageType(MessageType.PRIVATE);
        }
        return machineBuilder.build();
    }

    public MessageBuilder getBaseMessageBuilder() {
        MessageBuilder messageBuilder = new MessageBuilder();
        messageData = inputReader.getUserInput("Enter Title:");
        messageBuilder.withTitle(messageData);
        messageData = inputReader.getUserInput("Enter Author Name:");
        messageBuilder.withName(messageData);
        messageData = inputReader.getUserInput("Enter Author Surname:");
        messageBuilder.withSurname(messageData);
        messageData = inputReader.getUserInput("Enter Content:");
        messageBuilder.withContent(messageData);
        return messageBuilder;
    }

}
