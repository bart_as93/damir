package pl.cm.dj.repository;

import pl.cm.dj.model.Message;

import java.util.List;
import java.util.function.Predicate;

public interface MessageStorage {

    List<Message> getAll ();
    void add(Message message);
    boolean remove (Message message);
    boolean remove(Predicate<Message> predicate);
}
