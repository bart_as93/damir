package pl.cm.dj.repository;

import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;

import java.util.List;
import java.util.function.Predicate;

public interface MessageRepository {

    void add(Message message);

    boolean remove(Message message);

    List<Message> getAll();

    List<Message> findByFilters(Message messageCriteria);

    Predicate<Message> getContentPredicate(String content);

    Predicate<Message> getTitlePredicate(String messageTitle);

    Predicate<Message> getAuthorNamePredicate(String name);

    Predicate<Message> getAuthorSurnamePredicate(String surname);

}
