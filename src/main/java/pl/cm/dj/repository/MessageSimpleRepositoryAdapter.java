package pl.cm.dj.repository;

import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MessageSimpleRepositoryAdapter implements MessageRepository {

    private MessageSimpleRepository simpleRepository;

    public MessageSimpleRepositoryAdapter(MessageSimpleRepository simpleRepository) {
        this.simpleRepository = simpleRepository;
    }

    @Override
    public void add(Message message) {
        simpleRepository.add(message);
    }

    @Override
    public List<Message> getAll() {
        return simpleRepository.getAll();
    }

    @Override
    public boolean remove(Message message){
        Predicate<Message> composePredicate = getContentPredicate(message.getContent())
                .and(getTitlePredicate(message.getTitle()))
                .and(getAuthorNamePredicate(message.getAuthor().getName()))
                .and(getAuthorSurnamePredicate(message.getAuthor().getSurname()));
        return simpleRepository.remove(composePredicate);
    }

    @Override
    public List<Message> findByFilters(Message message) {
        Predicate<Message> composePredicate = getContentPredicate(message.getContent())
                .and(getTitlePredicate(message.getTitle()))
                .and(getAuthorNamePredicate(message.getAuthor().getName()))
                .and(getAuthorSurnamePredicate(message.getAuthor().getSurname()));
        return getAll().stream()
                .filter(composePredicate)
                .collect(Collectors.toList());
    }

    @Override
    public Predicate<Message> getContentPredicate(String content) {
        if(!content.equals("")){
            return item -> item.getContent().equals(content);
        } else {
            return x -> true;
        }
    }

    @Override
    public Predicate<Message> getTitlePredicate(String messageTitle) {
        if(!messageTitle.equals("")){
            return item -> item.getTitle().equals(messageTitle);
        } else {
            return x -> true;
        }
    }

    @Override
    public Predicate<Message> getAuthorNamePredicate(String name) {
        if(!name.equals("")){
            return item -> item.getAuthor().getName().equals(name);
        } else {
            return x -> true;
        }
    }

    @Override
    public Predicate<Message> getAuthorSurnamePredicate(String surname) {
        if(!surname.equals("")){
            return item -> item.getAuthor().getSurname().equals(surname);
        } else {
            return x -> true;
        }
    }
}
