package pl.cm.dj.repository;

import pl.cm.dj.model.Message;

import java.util.List;
import java.util.function.Predicate;

public class MessageSimpleRepository {

    private MessageSimpleStorage simpleStorage;

    public MessageSimpleRepository(MessageSimpleStorage simpleStorage) {
        this.simpleStorage = simpleStorage;
    }

    void add(Message message){
        simpleStorage.add(message);
    }

    boolean remove(Message message){
        return simpleStorage.remove(message);
    }

    boolean remove(Predicate<Message> predicate){
        return simpleStorage.remove(predicate);
    }

    List<Message> getAll() {
        return simpleStorage.getAll();
    }

}
