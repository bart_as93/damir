package pl.cm.dj.repository;

import pl.cm.dj.model.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class MessageSimpleStorage implements MessageStorage{

    public MessageSimpleStorage() {
        super();
    }

    private List<Message> dataStorage = new ArrayList<>();

    @Override
    public List<Message> getAll() {
        return dataStorage;
    }

    @Override
    public void add(Message message) {
        if (message == null){
            throw new RuntimeException();
        }
        dataStorage.add(message);
    }

    @Override
    public boolean remove(Message message) {
        return dataStorage.remove(message);
    }

    @Override
    public boolean remove(Predicate<Message> predicate){
        return dataStorage.removeIf(predicate);
    }
}
