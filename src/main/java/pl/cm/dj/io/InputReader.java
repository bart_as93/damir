package pl.cm.dj.io;

import java.util.Scanner;

public class InputReader {

    private static Scanner scanner = new Scanner(System.in);

    public String getUserInput(String messageForUser){
        System.out.println(messageForUser);
        return scanner.nextLine();
    }
}
