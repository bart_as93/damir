package pl.cm.dj.service;

import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageType;
import pl.cm.dj.repository.MessageRepository;

import java.time.LocalDateTime;

public class MessageFillerService {

    private MessageRepository messageRepository;

    public MessageFillerService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void fillRepository(int amountOfMessages) {
        for (int i = 0; i < amountOfMessages; i++) {
            messageRepository.add(createSimpleMessage(i));
        }
    }

    public Message createSimpleMessage(int suffix) {
        AuthorOfMessage author = new AuthorOfMessage();
        author.setName("AuthorName" + suffix);
        author.setSurname("AuthorSurname" + suffix);
        Message message = new Message();
        message.setTitle("Title" + suffix);
        message.setAuthor(author);
        message.setContent("Content" + suffix);
        message.setCreationDate(LocalDateTime.now());
        message.setType(getRandomMessageType());
        return message;
    }

    public MessageType getRandomMessageType() {
        if ((int) (Math.random() * 2) < 1) {
            return MessageType.IMPORTANT;
        } else {
            return MessageType.PRIVATE;
        }
    }
}
