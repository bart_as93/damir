package pl.cm.dj.service;

import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.repository.MessageRepository;

import java.util.List;
import java.util.function.Predicate;

public class MessageService {

    private MessageRepository repository;

    public MessageService(MessageRepository repository) {
        this.repository = repository;
    }

    public boolean addIfNotExists(Message message) {
        if (repository.getAll()
                .stream()
                .anyMatch(m -> m == message)) {
            return false;
        }
        repository.add(message);
        return true;
    }

    public boolean deleteByTitle(String title) {
        boolean delete = false;
        for (Message message : repository.getAll()) {
            if (message.getTitle().equals(title)) {
                repository.remove(message);
                delete = true;
            }
        }

        return delete;

    }

    public boolean remove(Message messageCriteria){
        return repository.remove(messageCriteria);
    }

    public List<Message> findByFilters(Message messageCriteria) {
        return repository.findByFilters(messageCriteria);
    }

}
