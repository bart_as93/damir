package pl.cm.dj.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Message {

    private String title;
    private AuthorOfMessage author;
    private String content;
    private LocalDateTime creationDate;
    private MessageType type;

    public Message() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AuthorOfMessage getAuthor() {
        return author;
    }

    public void setAuthor(AuthorOfMessage author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return title.equals(message.title) &&
                author.equals(message.author) &&
                content.equals(message.content) &&
                Objects.equals(type, message.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, content, content, type);
    }

    @Override
    public String toString() {
        return "Message{" +
                "title='" + title + '\'' +
                ", author=" + author +
                ", content='" + content + '\'' +
                ", creationDate=" + creationDate +
                ", type=" + type +
                '}';
    }

    public String toSimpleString() {
        return "Message{" +
                "title='" + title + '\'' +
                ", author=" + author +
                '}';
    }


}

