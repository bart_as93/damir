package pl.cm.dj.model;

public enum MessageType {

    IMPORTANT, PRIVATE;
}
