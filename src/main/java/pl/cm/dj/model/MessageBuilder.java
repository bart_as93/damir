package pl.cm.dj.model;

import java.time.LocalDateTime;

public class MessageBuilder {

    private String title, name, surname, content;
    private LocalDateTime creationDate;
    private MessageType type;

    public MessageBuilder withTitle(String title){
        this.title = title;
        return this;
    }

    public MessageBuilder withName(String name){
        this.name = name;
        return this;
    }

    public MessageBuilder withSurname(String surname){
        this.surname = surname;
        return this;
    }

    public MessageBuilder withContent(String content){
        this.content = content;
        return this;
    }

    public MessageBuilder withCreationDate(LocalDateTime creationDate){
        this.creationDate = creationDate;
        return this;
    }

    public MessageBuilder withMessageType (MessageType type){
        this.type = type;
        return this;
    }

    public Message build(){
        Message message = new Message();
        AuthorOfMessage author = new AuthorOfMessage();
        message.setTitle(this.title);
        author.setName(this.name);
        author.setSurname(this.surname);
        message.setAuthor(author);
        message.setContent(content);
        message.setCreationDate(creationDate);
        message.setType(type);
        return message;
    }

}
