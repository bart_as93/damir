package pl.cm.dj.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageType;
import pl.cm.dj.repository.MessageRepository;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageFillerServiceTest {

    @InjectMocks
    private MessageFillerService messageFillerService;

    @Mock
    private MessageRepository messageRepository;

    @Test
    public void messageFillerServiceNotNull() {
        assertNotNull(messageFillerService);
        assertThat(messageRepository.getAll().isEmpty(), is(true));
    }

    @Test
    public void messageIsCreated() {
        List<Message> messages = new ArrayList<>();
        messages.add(messageFillerService.createSimpleMessage(2));
        messages.add(messageFillerService.createSimpleMessage(1));

        assertEquals(messages.size(), 2);
        assertThat(messages.size(), is(2));

    }
    @Test
    public void getRandomMesseageTest() {
        List<MessageType> messageTypes = new ArrayList<>();

        for(int i =0; i<100;i++){
            messageTypes.add(messageFillerService.getRandomMessageType());
        }

        assertTrue(messageTypes.contains(MessageType.IMPORTANT));
        assertTrue(messageTypes.contains(MessageType.PRIVATE));
    }
}