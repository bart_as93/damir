package pl.cm.dj.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageType;
import pl.cm.dj.repository.MessageRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)

public class MessageServiceTest {

    @InjectMocks
    private MessageService messageService;

    @Mock
    private MessageRepository repository;

    @Test
    public void addIfNotExist_MessageNotExists(){
        // given
        List<Message> messages = createMessages();
        when(repository.getAll()).thenReturn(messages);
        Message messageToAdd = createMessage("Title2", "authorName2", "authorSurname2", "Content1"
                , LocalDateTime.of(2019,2,12,10,10,10), MessageType.IMPORTANT);

        // when
        boolean result = messageService.addIfNotExists(messageToAdd);

        // then
        assertTrue(result);
    }

    @Test
    public void addIfNotExist_MessageExists(){
        // given
        List<Message> messages = createMessages();
        when(repository.getAll()).thenReturn(messages);
        Message messageToAdd = messages.get(0);
        // when
        boolean result = messageService.addIfNotExists(messageToAdd);

        // then
        assertFalse(result);
    }

    @Test(expected = NullPointerException.class)
    public void addIfNotExist_MessagesIsNull(){
        // given
        List<Message> messages = null;
        when(repository.getAll()).thenReturn(messages);
        Message messageToAdd = createMessage("Title2", "authorName2", "authorSurname2", "Content1"
                , LocalDateTime.of(2019,2,12,10,10,10), MessageType.IMPORTANT);

        // when
        boolean result = messageService.addIfNotExists(messageToAdd);

        // then
        assertFalse(result);
    }
    @Test
    public void deleteByTitle_messageExists(){
        // given
        List<Message> messages = createMessages();
        when(repository.getAll()).thenReturn(messages);

        // when
        boolean result = messageService.deleteByTitle("Title1");

        // then
        assertTrue(result);
    }
    @Test
    public void deleteByTitle_messageNotExists(){
        // given
        List<Message> messages = createMessages();
        when(repository.getAll()).thenReturn(messages);


        // when
        boolean result = messageService.deleteByTitle("Title100");

        // then
        assertFalse(result);
    }
    @Test
    public void deleteByTitle_messageIsNull(){
        // given
        List<Message> messages = createMessages();
        when(repository.getAll()).thenReturn(messages);


        // when
        boolean result = messageService.deleteByTitle(null);

        // then
        assertFalse(result);
    }
    @Test
    public void testRemoveMessage() {
        List<Message> afterRemove = repository.getAll();

        repository.remove(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(afterRemove.size(), 0);
        assertThat(afterRemove.size(), is(0));

    }
    @Test
    public void testRemoveMessageNotExist() {
        List<Message> afterRemove = createMessages();

        repository.remove(createMessage("Title2", "authorName2", "authorSurname2", "Content2"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(afterRemove.size(), 1);
        assertThat(afterRemove.size(), is(1));

    }
    @Test
    public void testRemoveMessageIsNull(){
        // given
        List<Message> messages = createMessages();
        when(repository.getAll()).thenReturn(messages);

        // when
        boolean result = messageService.remove(null);

        // then
        assertFalse(result);
        assertThat(messages.size(), is(1));
        assertEquals(messages.size(), 1);
    }
    @Test
    public void testFilterMessage() {
        List<Message> afterFilter = repository.getAll();

        repository.findByFilters(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(afterFilter.size(), 0);
        assertThat(afterFilter.size(), is(0));

    }
    @Test
    public void testFilterMessageNotExist() {
        List<Message> afterFilter = createMessages();

        messageService.findByFilters(createMessage("Title2", "authorName2", "authorSurname2", "Content2"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(afterFilter.size(), 1);
        assertThat(afterFilter.size(), is(1));

    }

    private List<Message> createMessages() {
        List<Message> messages = new ArrayList<>();
        messages.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019,2,12,10,10,10), MessageType.IMPORTANT));
        return messages;
    }

    private Message createMessage(String title, String authorName, String authorSurname, String content,
                                  LocalDateTime creationDate, MessageType type) {
        AuthorOfMessage author = new AuthorOfMessage();
        author.setName(authorName);
        author.setSurname(authorSurname);
        Message message = new Message();
        message.setTitle(title);
        message.setAuthor(author);
        message.setContent(content);
        message.setCreationDate(creationDate);
        message.setType(type);
        return message;
    }

}