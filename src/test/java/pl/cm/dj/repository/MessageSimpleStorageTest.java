package pl.cm.dj.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageSimpleStorageTest {

    private MessageSimpleStorage messageSimpleStorage = new MessageSimpleStorage();

    @Test
    public void testGetAllMessages() {
        assertNotNull(messageSimpleStorage);
        assertThat(messageSimpleStorage.getAll().isEmpty(), is(true));
    }

    @Test
    public void testGetAllCreatedMessages() {
        List<Message> messages = createMessages();

        assertNotNull(messages);
        assertThat(messages.isEmpty(), is(false));
    }

    @Test
    public void messageWhenCreated() {
        assertNotNull("Message should exist", createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        assertThat("Message should be empty", messageSimpleStorage.getAll().isEmpty(), is(true));
    }

    @Test
    public void addWhenMessageProvided() {
        List<Message> messages = createMessages();
        assertThat(messages.size(), is(1));
        assertThat(messages, hasItem(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT)));
    }

    @Test
    public void testAddMessage() {
        List<Message> beforeAdd = messageSimpleStorage.getAll();

        messageSimpleStorage.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));


        assertEquals(beforeAdd.size(), 1);
    }

    @Test
    public void testRemoveMessage() {
        List<Message> beforeRemove = messageSimpleStorage.getAll();

        messageSimpleStorage.remove(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(beforeRemove.size(), 0);
        assertThat(beforeRemove.size(), is(0));

    }

    @Test
    public void testRemovePredicateMessage() {
        messageSimpleStorage.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        messageSimpleStorage.add(createMessage("Title2", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        int beforeRemoveListSize = messageSimpleStorage.getAll().size();
        Predicate<Message> testPredicate = message -> message.getTitle().equals("Title1");

        boolean result = messageSimpleStorage.remove(testPredicate);
        List<Message> afterRemove = messageSimpleStorage.getAll();

        assertEquals(result, true);
        assertEquals(beforeRemoveListSize,2);
        assertEquals(afterRemove.size(),1);
        assertEquals(afterRemove.get(0).getTitle(),"Title2");
    }


    private List<Message> createMessages() {
        List<Message> messages = new ArrayList<>();
        messages.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        return messages;
    }

    private Message createMessage(String title, String authorName, String authorSurname, String content,
                                  LocalDateTime creationDate, MessageType type) {
        AuthorOfMessage author = new AuthorOfMessage();
        author.setName(authorName);
        author.setSurname(authorSurname);
        Message message = new Message();
        message.setTitle(title);
        message.setAuthor(author);
        message.setContent(content);
        message.setCreationDate(creationDate);
        message.setType(type);
        return message;
    }

}
