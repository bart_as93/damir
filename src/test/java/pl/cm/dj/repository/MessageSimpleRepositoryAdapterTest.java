package pl.cm.dj.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageType;
import pl.cm.dj.service.MessageFillerService;
import pl.cm.dj.service.MessageService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageSimpleRepositoryAdapterTest {

    @InjectMocks
    private MessageSimpleRepositoryAdapter messageSimpleRepositoryAdapter;

    @Mock
    private MessageSimpleRepository simpleRepository;

    @Test
    public void testGetAllMessages() {
        assertNotNull(messageSimpleRepositoryAdapter);
        assertThat(messageSimpleRepositoryAdapter.getAll().isEmpty(), is(true));
    }

    @Test
    public void testGetAllCreatedMessages() {
        List<Message> messages = createMessages();

        assertNotNull(messages);
        assertThat(messages.isEmpty(), is(false));
    }

    @Test
    public void messageWhenCreated() {
        assertNotNull("Message should exist", createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        assertThat("Message should be empty", messageSimpleRepositoryAdapter.getAll().isEmpty(), is(true));
    }

    @Test
    public void addWhenMessageProvided() {
        List<Message> messages = createMessages();
        assertThat(messages.size(), is(1));
        assertThat(messages, hasItem(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT)));
    }

    @Test
    public void testAddMessage() {
        List<Message> afterAdd = createMessages();

        messageSimpleRepositoryAdapter.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));


        assertEquals(afterAdd.size(), 1);
    }

    @Test
    public void testRemoveMessage() {
        List<Message> afterRemove = messageSimpleRepositoryAdapter.getAll();

        messageSimpleRepositoryAdapter.remove(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(afterRemove.size(), 0);
        assertThat(afterRemove.size(), is(0));

    }
    @Test
    public void testEqualRemoving() {
        // given
        List<Message> messages = createMessages();
        when(messageSimpleRepositoryAdapter.getAll()).thenReturn(messages);

        // when
        messageSimpleRepositoryAdapter
                .remove(createMessage("", "", "", "Content1", null, null));

        // then
        assertEquals(1, messages.size());
    }

    @Test
    public void testEqualRemovingDifferentValues() {
        // given
        List<Message> messages = createMessages();
        when(messageSimpleRepositoryAdapter.getAll()).thenReturn(messages);

        // when
        messageSimpleRepositoryAdapter
                .remove(createMessage("", "", "", "Content2", null, null));

        // then
        assertNotEquals(0, messages.size());
    }

    @Test
    public void testNotEqualRemoving() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
       messageSimpleRepositoryAdapter
                .remove(createMessage("", "", "", "Content2", null, null));

        // then
        assertEquals(1, messages.size());
    }

    @Test
    public void testContentPredicate() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getContentPredicate("Content1");
        boolean result = predicate.test(messages.get(0));

        // then
        assertTrue(result);
    }

    @Test
    public void testContentPredicateFailed() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getContentPredicate("Content2");
        boolean result = predicate.test(messages.get(0));

        // then
        assertFalse(result);
    }

    @Test
    public void testTitlePredicate() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getTitlePredicate("Title1");
        boolean result = predicate.test(messages.get(0));

        // then
        assertTrue(result);
    }

    @Test
    public void testTitlePredicateFailed() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getTitlePredicate("Title2");
        boolean result = predicate.test(messages.get(0));

        // then
        assertFalse(result);
    }

    @Test
    public void testAuthorNamePredicate() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getAuthorNamePredicate("authorName1");
        boolean result = predicate.test(messages.get(0));

        // then
        assertTrue(result);
    }

    @Test
    public void testAuthorNamePredicateFailed() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getAuthorNamePredicate("authorName2");
        boolean result = predicate.test(messages.get(0));

        // then
        assertFalse(result);
    }

    @Test
    public void testAuthorSurnamePredicate() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getAuthorSurnamePredicate("authorSurname1");
        boolean result = predicate.test(messages.get(0));

        // then
        assertTrue(result);
    }

    @Test
    public void testAuthorSurnamePredicateFailed() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        Predicate<Message> predicate = messageSimpleRepositoryAdapter.getAuthorSurnamePredicate("AuthorSurname2");
        boolean result = predicate.test(messages.get(0));

        // then
        assertFalse(result);
    }

    @Test
    public void testEqualFiltering() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        List<Message> result = messageSimpleRepositoryAdapter
                .findByFilters(createMessage("", "", "", "Content1", null, null));

        // then
        assertEquals(1, result.size());
    }

    @Test
    public void testEqualFilteringDifferentValues() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        List<Message> result = messageSimpleRepositoryAdapter
                .findByFilters(createMessage("", "", "", "Content2", null, null));

        // then
        assertEquals(0, result.size());
    }

    @Test
    public void testNotEqualFiltering() {
        // given
        List<Message> messages = createMessages();
        when(simpleRepository.getAll()).thenReturn(messages);

        // when
        List<Message> result = messageSimpleRepositoryAdapter
                .findByFilters(createMessage("", "", "", "Content2", null, null));

        // then
        assertNotEquals(1, result.size());
    }

    private List<Message> createMessages() {
        List<Message> messages = new ArrayList<>();
        messages.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        return messages;
    }


    private Message createMessage(String title, String authorName, String authorSurname, String content,
                                  LocalDateTime creationDate, MessageType type) {
        AuthorOfMessage author = new AuthorOfMessage();
        author.setName(authorName);
        author.setSurname(authorSurname);
        Message message = new Message();
        message.setTitle(title);
        message.setAuthor(author);
        message.setContent(content);
        message.setCreationDate(creationDate);
        message.setType(type);
        return message;
    }

}