package pl.cm.dj.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.model.AuthorOfMessage;
import pl.cm.dj.model.Message;
import pl.cm.dj.model.MessageType;
import pl.cm.dj.service.MessageService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageSimpleRepositoryTest {
    @InjectMocks
    private MessageSimpleRepository messageSimpleRepository;

    @Mock
    private MessageSimpleStorage simpleStorage;


    @Test
    public void testGetAllMessages() {
        assertNotNull(messageSimpleRepository);
        assertNotNull(simpleStorage);
        assertThat(messageSimpleRepository.getAll().isEmpty(), is(true));
    }

    @Test
    public void testGetAllCreatedMessages() {
        List<Message> messages = createMessages();

        assertNotNull(messages);
        assertThat(messages.isEmpty(), is(false));
    }

    @Test
    public void messageWhenCreated() {
        assertNotNull("Message should exist", createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        assertThat("Message should be empty", messageSimpleRepository.getAll().isEmpty(), is(true));
    }

    @Test
    public void addWhenMessageProvided() {
        List<Message> messages = createMessages();
        assertThat(messages.size(), is(2));
        assertThat(messages, hasItem(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT)));
    }

    @Test
    public void testAddMessage() {
        List<Message> afterAdd = createMessages();

        messageSimpleRepository.add(createMessage("Title0", "authorName0", "authorSurname0", "Content0"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        messageSimpleRepository.add(createMessage("Title2", "authorName2", "authorSurname2", "Content2"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));


        assertEquals(afterAdd.size(), 2);
        assertThat(afterAdd.size(), is(2));
    }

    @Test
    public void testRemoveMessage() {
        List<Message> afterRemove = messageSimpleRepository.getAll();

        messageSimpleRepository.remove(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));

        assertEquals(afterRemove.size(), 0);
        assertThat(afterRemove.size(), is(0));

    }

//    @Test
//    public void testRemovePredicateMessage() {
//        List<Message> afterRemove = messageSimpleRepository.getAll();
//
//        messageSimpleRepository.remove();
//
//        assertNotEquals(afterRemove.size(), 1);
//        assertThat(afterRemove.size(), is(0));
//
//    }


    private List<Message> createMessages() {
        List<Message> messages = new ArrayList<>();
        messages.add(createMessage("Title1", "authorName1", "authorSurname1", "Content1"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        messages.add(createMessage("Title2", "authorName2", "authorSurname2", "Content2"
                , LocalDateTime.of(2019, 2, 12, 10, 10, 10), MessageType.IMPORTANT));
        return messages;
    }

    private Message createMessage(String title, String authorName, String authorSurname, String content,
                                  LocalDateTime creationDate, MessageType type) {
        AuthorOfMessage author = new AuthorOfMessage();
        author.setName(authorName);
        author.setSurname(authorSurname);
        Message message = new Message();
        message.setTitle(title);
        message.setAuthor(author);
        message.setContent(content);
        message.setCreationDate(creationDate);
        message.setType(type);
        return message;
    }

}