package pl.cm.dj;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.io.InputReader;
import pl.cm.dj.model.Message;
import pl.cm.dj.repository.MessageSimpleRepository;
import pl.cm.dj.repository.MessageSimpleRepositoryAdapter;
import pl.cm.dj.repository.MessageSimpleStorage;
import pl.cm.dj.service.MessageFillerService;
import pl.cm.dj.service.MessageService;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainTest {
    @InjectMocks
    private Main main;
    @Mock
    private MessageSimpleStorage storage;
    @Mock
    private MessageSimpleRepository simpleRepository;
    @Mock
    private MessageSimpleRepositoryAdapter simpleRepositoryAdapter;
    @Mock
    private MessageService messageService;
    @Mock
    private InputReader inputReader;
    @Mock
    private  MessageFillerService messageFillerService;

    @Test
    public void userBaseInteraction(){
        boolean result = main.userBaseInteraction("1");

        assertTrue(result);
    }

    @Test
    public void userBaseInteractionExit(){
        boolean result = main.userBaseInteraction("6");

        assertFalse(result);
    }

    @Test
    public void filterMessagesTest(){
        main.filterMessages();

        verify(inputReader, times(4)).getUserInput(anyString());
    }

    @Test
    public void deleteMessage(){
        main.deleteMessage();

        verify(messageService, times(1)).remove(any());
        verify(inputReader, times(4)).getUserInput(anyString());
    }

    @Test
    public void deleteByMessagePropertiesThatAreNotNull(){
        when(messageService.remove(null)).thenReturn(true);

        boolean result = main.deleteByMessagePropertiesThatAreNotNull(null);

        assertTrue(result);
    }

    //TODO pozmieniaj nazwy metod, jeśli bedzie Ci sie chciało to możesz jeszcze poszukać nie przetestowanych metod które albo coś
    //zwracają albo wywołują jakąś metode na serwisie (lub innych klasach z adnotacją @Mock w teście). To wtedy możesz użyć when(), verrify



}