package pl.cm.dj.io;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class InputReaderTest {
    @InjectMocks
    InputReader inputReader;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Mock
    private PrintStream printStream;

    @Test
    public void should_GetUserInput() {
        System.setOut(printStream);

        System.out.println("something to test");

        Mockito.verify(printStream).println(stringCaptor.capture());
        Assertions.assertThat("something to test").isEqualTo(stringCaptor.getValue());
        assertNotNull(inputReader);

    }

}