package pl.cm.dj.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.doCallRealMethod;

@RunWith(MockitoJUnitRunner.class)
public class MessageTest {
    @InjectMocks
    private Message message;

    @Test
    public void messageWhenTitleNotProvided() {
        String title = "Title0";
        Assert.assertNotEquals("title not provided", title, message.getTitle());
        Assert.assertNull(message.getTitle());
    }

    @Test
    public void messageWhenTitleProvided() {
        message.setTitle("Title11");
        Assert.assertEquals("title set properly","Title11", message.getTitle());
    }

    @Test
    public void messageWhenAuthorNotProvided() {
        String author = "authorName1 authorSurname1";
        Assert.assertNotEquals("author not provided", author, message.getAuthor());
        Assert.assertNull(message.getAuthor());
    }
    @Test
    public void messageWhenAuthorProvided() {
        AuthorOfMessage author = new AuthorOfMessage();
        author.setName("Name1");
        author.setSurname("Surname1");
        message.setAuthor(author);
        Assert.assertEquals("title set properly",author, message.getAuthor());
    }

    @Test
    public void messageWhenContentNotProvided() {
        String content = "Content1";
        Assert.assertNotEquals("content not provided", content, message.getTitle());
        Assert.assertNull(message.getContent());
    }
    @Test
    public void messageWhenContentProvided() {
        message.setContent("Content11");
        Assert.assertEquals("content set properly","Content11", message.getContent());
    }

    @Test
    public void messageWhenDataProvided() {
        Assert.assertNull(message.getCreationDate());
    }

    @Test
    public void messageWhenTypeNotProvided() {
        MessageType type = MessageType.IMPORTANT;
        Assert.assertNotEquals("type not provided", type, message.getType());
        Assert.assertNull(message.getType());
    }

    @Test
    public void messageWhenTypeProvided() {
        message.setType(MessageType.IMPORTANT);
        Assert.assertEquals("type set properly", MessageType.IMPORTANT, message.getType());
    }

    @Test
    public void equalsWhenTheSameMessages() {
        Message message1 = new Message();
        Message message2 = message1;
        boolean ret = message1.equals(message2);
        Assert.assertTrue("objects should be the same", ret);
    }

    @Test
    public void hashCodeWhenSameValue() {
        Message message1 = new Message();
        Message message2 = new Message();
        int hash1 = message1.hashCode();
        int hash2 = message2.hashCode();
        Assert.assertTrue("hash codes should be the same", hash1 == hash2);
    }
    @Test
    public void testToString() {

        String toString = message.toString();

        Assert.assertEquals("Title0", "Title0");
        Assert.assertEquals("test", "Message{title='null', author=null, content='null', creationDate=null, type=null}", toString);
    }
    @Test
    public void testToSimpleSTring() {

        String toSimpleString = message.toSimpleString();

        Assert.assertEquals("Title0", "Title0");
        Assert.assertEquals("string wrong formatter", "Message{title='null', author=null}", toSimpleString);
    }

}