package pl.cm.dj.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorOfMessageTest {

    @InjectMocks
    private AuthorOfMessage author;

    @Test
    public void messageNameNotProvided() {
        String name = "authorName1";
        Assert.assertNotEquals("name not set", name, author.getName());
        Assert.assertNull(author.getName());
    }

    @Test
    public void messageNameProvided() {
        author.setName("authorName1");
        Assert.assertEquals("name set properly","authorName1", author.getName());
        Assert.assertNotNull(author.getName());
    }
    @Test
    public void messageSurnameNotProvided() {
        String surname = "authorSurname1";
        Assert.assertNotEquals("surname not set", surname, author.getSurname());
        Assert.assertNull(author.getSurname());
    }

    @Test
    public void messageSurnameProvided() {
        author.setSurname("authorSurname1");
        Assert.assertEquals("surname set properly","authorSurname1", author.getSurname());
        Assert.assertNotNull(author.getSurname());
    }


    @Test
    public void equalsWhenTheSameReference() {
        AuthorOfMessage author1 = new AuthorOfMessage();
        AuthorOfMessage author2 = author1;
        boolean ret = author1.equals(author2);
        Assert.assertTrue("authors should be the same", ret);
    }

    @Test
    public void hashCodeWhenSameValue() {
        AuthorOfMessage author1 = new AuthorOfMessage();
        AuthorOfMessage author2 = new AuthorOfMessage();
        int hash1 = author1.hashCode();
        int hash2 = author2.hashCode();
        Assert.assertTrue("hash codes should be the same", hash1 == hash2);
    }
    @Test
    public void testToString() {

        String toString = author.toString();

        Assert.assertEquals("Name0", "Name0");
        Assert.assertEquals("test", "null null", toString);
    }

}