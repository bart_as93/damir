package pl.cm.dj.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.dj.repository.MessageSimpleRepositoryAdapter;

import static org.junit.Assert.*;

public class MessageBuilderTest {
    @Test
    public void testMessageBuilderWithTitle() {
        Message message = new MessageBuilder().withTitle("title").build();

        assertEquals(message.getTitle(), "title");
        assertEquals(message.getContent(), null);
    }

    //TODO Dorobic do reszty metod tak samo jak powyzej
}